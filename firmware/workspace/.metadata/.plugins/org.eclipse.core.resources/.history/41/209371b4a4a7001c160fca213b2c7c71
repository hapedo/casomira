#include <string.h>
#include <ubeam/drivers/interrupt_ctrl.h>
#include <ubeam/utils/system_time.h>
#include <ubeam/drivers/power_management.h>
#include <ubeam/drivers/uart.h>

typedef enum
{
	UST_SYNC = 0,
	UST_COMMAND,
	UST_TYPE,
	UST_DATA
} UartState;

typedef enum
{
	UCMD_UPDATE = 0,
	UCMD_PRESENCE
} UartCommand;

typedef enum
{
	UCT_SET = 0,
	UCT_GET
} UartCommandType;

static UBGpioPin pinLedAct;

static UBGpioPin pinUartTx;
static UBGpioPin pinUartRx;

static UBFifo txFifo;
static UBFifo rxFifo;
static uint8_t txBuffer[64];
static uint8_t rxBuffer[128];

static UBUart uart;

static UBGpioPin pinDigits[8];
static UBGpioPin pinDigitIndexes[4];

static uint8_t digitIndex;
static uint8_t digits[4];

static UartState uartState;
static UBFifo uartData;
static uint8_t dataBuffer[16];
static UartCommand uartCommand;
static UartCommandType uartCommandType;

void TIM14_IRQHandler()
{
	TIM14->SR = 0;

	for(uint8_t i = 0; i < 4; i++)
	{
		UBGpioPin_Clear(&pinDigitIndexes[i]);
	}

	digitIndex++;
	if (digitIndex >= 4)
		digitIndex = 0;

	uint8_t val = digits[digitIndex];
	for(uint8_t i = 0; i < 8; i++)
	{
		if (val & (1 << i))
			UBGpioPin_Set(&pinDigits[i]);
		else
			UBGpioPin_Clear(&pinDigits[i]);
	}

	UBGpioPin_Set(&pinDigitIndexes[digitIndex]);
}

void uartSendAck()
{
	uint8_t data[6] = {'>', ' ', '!', 'O', 'K', ';'};
	if (uartCommand == UCMD_UPDATE)
		data[1] = 'S';
	if (uartCommand == UCMD_PRESENCE)
		data[1] = 'P';
	UBUart_Enqueue(&uart, data, 6, 0);
	UBUart_Transmit(&uart);
}

void uartPerformCommand()
{
	if (uartCommand == UCMD_UPDATE)
	{
		if (UBFifo_GetCount(&uartData) == 8)
		{
			for (uint8_t i = 0; i < 4; i++)
			{
				uint8_t val = 0;
				uint8_t d = 0;
				UBFifo_GetByte(&uartData, &d);
				if ((d >= '0') && (d <= '9'))
					val = (d - '0') << 4;
				if ((d >= 'A') && (d <= 'F'))
					val = (d - 'A' + 10) << 4;
				UBFifo_GetByte(&uartData, &d);
				if ((d >= '0') && (d <= '9'))
					val |= (d - '0');
				if ((d >= 'A') && (d <= 'F'))
					val |= (d - 'A' + 10);
				digits[i] = val;
			}
		}
	}
	uartSendAck();
}

void uartProcess()
{
	uint8_t data;
	while(UBFifo_GetByte(&rxFifo, &data))
	{
		if (data == '>')
		{
			uartState = UST_COMMAND;
			continue;
		}
		switch(uartState)
		{
		case UST_COMMAND:
			uartState = UST_TYPE;
			if (data == 'S')
				uartCommand = UCMD_UPDATE;
			else if (data == 'P')
				uartCommand = UCMD_PRESENCE;
			else
				uartState = UST_SYNC;
			break;
		case UST_TYPE:
			uartState = UST_DATA;
			UBFifo_Clear(&uartData);
			if (data == '?')
				uartCommandType = UCT_GET;
			else if (data == '!')
				uartCommandType = UCT_SET;
			else
				uartState = UST_SYNC;
			break;
		case UST_DATA:
			if (data == ';')
			{
				uartState = UST_SYNC;
				uartPerformCommand();
			}
			else
				UBFifo_PutByte(&uartData, data);
			break;
		default:
			break;
		}
	}
}

int main()
{
	UBSysTime_Configure();
	UBInt_EnableGlobal();

	UBGpioPin_Configure(&pinLedAct, (UBPeriHandle)GPIOB_BASE, UBGpioPin5);
	UBPower_EnablePower(pinLedAct.port.base.handle);
	UBGpioPin_Output(&pinLedAct);

	UBGpioPin_Configure(&pinUartTx, (UBPeriHandle)GPIOB_BASE, UBGpioPin6);
	UBGpioPin_Configure(&pinUartRx, (UBPeriHandle)GPIOB_BASE, UBGpioPin7);

	UBPower_EnablePower(pinUartTx.port.base.handle);
	UBPower_EnablePower(pinUartRx.port.base.handle);

	UBFifo_Configure(&txFifo, txBuffer, sizeof(txBuffer));
	UBFifo_Configure(&rxFifo, rxBuffer, sizeof(rxBuffer));
	UBFifo_Configure(&uartData, dataBuffer, sizeof(dataBuffer));

	UBUart_Configure(&uart, (UBPeriHandle)USART1, &pinUartRx, &pinUartTx, &rxFifo, &txFifo);
	UBUart_Init(&uart, 19200);

	UBPower_EnablePower(pinUartTx.port.base.handle);

	UBGpioPin_Configure(&pinDigits[0], (UBPeriHandle)GPIOA_BASE, UBGpioPin0);
	UBGpioPin_Configure(&pinDigits[1], (UBPeriHandle)GPIOA_BASE, UBGpioPin1);
	UBGpioPin_Configure(&pinDigits[2], (UBPeriHandle)GPIOA_BASE, UBGpioPin2);
	UBGpioPin_Configure(&pinDigits[3], (UBPeriHandle)GPIOA_BASE, UBGpioPin3);
	UBGpioPin_Configure(&pinDigits[4], (UBPeriHandle)GPIOA_BASE, UBGpioPin4);
	UBGpioPin_Configure(&pinDigits[5], (UBPeriHandle)GPIOA_BASE, UBGpioPin5);
	UBGpioPin_Configure(&pinDigits[6], (UBPeriHandle)GPIOA_BASE, UBGpioPin6);
	UBGpioPin_Configure(&pinDigits[7], (UBPeriHandle)GPIOA_BASE, UBGpioPin7);
	UBGpioPin_Configure(&pinDigitIndexes[0], (UBPeriHandle)GPIOA_BASE, UBGpioPin8);
	UBGpioPin_Configure(&pinDigitIndexes[1], (UBPeriHandle)GPIOA_BASE, UBGpioPin9);
	UBGpioPin_Configure(&pinDigitIndexes[2], (UBPeriHandle)GPIOA_BASE, UBGpioPin10);
	UBGpioPin_Configure(&pinDigitIndexes[3], (UBPeriHandle)GPIOA_BASE, UBGpioPin11);
	UBPower_EnablePower(pinDigits[0].port.base.handle);

	for(uint8_t i = 0; i < 8; i++)
	{
		UBGpioPin_Output(&pinDigits[i]);
		UBGpioPin_Clear(&pinDigits[i]);
	}

	for(uint8_t i = 0; i < 4; i++)
	{
		UBGpioPin_Output(&pinDigitIndexes[i]);
		UBGpioPin_Clear(&pinDigitIndexes[i]);
	}

	digitIndex = 0;
	digits[0] = 0;
	digits[1] = 0xc0;
	digits[2] = 0x40;
	digits[3] = 0x40;

/*	digits[0] = 0x02;
	digits[1] = 0xff;
	digits[2] = 0xff;
	digits[3] = 0xff;*/

	UBPower_EnablePower(TIM14);
	TIM14->PSC = 80;
	TIM14->DIER = 0x01;
	TIM14->ARR = 100;
	TIM14->CNT = 0;
	TIM14->CR1 = 0x01;
	UBInt_EnableInt(TIM14_IRQn);

	UBTimestamp blinkTimer;
	blinkTimer = UBSysTime_GetFutureMilli(500);

	uartState = UST_SYNC;

	while(1)
	{
		if (UBSysTime_HasPassed(blinkTimer))
		{
			blinkTimer = UBSysTime_GetFutureMilli(500);
			UBGpioPin_Invert(&pinLedAct);
		}
		uartProcess();
	}
}
