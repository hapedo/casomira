#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QDesktopServices>
#include "appconfig.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(&m_profile, SIGNAL(signalViewChanged(QString, QString)),
            this, SLOT(slotProfileViewChanged(QString, QString)));
    connect(&m_profile, SIGNAL(signalCountingStarted()),
            this, SLOT(slotProfileStarted()));
    connect(&m_profile, SIGNAL(signalCountingStopped()),
            this, SLOT(slotProfileStopped()));
    connect(&m_profile, SIGNAL(signalActionTaken(int)),
            this, SLOT(slotProfileActionTaken(int)));
    connect(&m_comm, SIGNAL(signalIsPresentChanged(bool)),
            this, SLOT(slotCommIsPresentChanged(bool)));
    connect(&m_tableWidget, SIGNAL(signalActionIndexChanged(int)),
            this, SLOT(slotActionTableIndexChanged(int)));
    connect(&m_tableWidget, SIGNAL(signalActionDoubleClick(int)),
            this, SLOT(slotActionTableDoubleClick(int)));

    m_config.loadFromFile();

    ui->btnAddAction->setDefaultAction(ui->actionAddAction);
    ui->btnEditAction->setDefaultAction(ui->actionEditAction);
    ui->btnRemoveAction->setDefaultAction(ui->actionDeleteAction);
    ui->btnStartFrom->setDefaultAction(ui->actionStartFrom);

    QPalette pal;
    pal.setColor(QPalette::WindowText, Qt::red);
    ui->labelConnected->setPalette(pal);

    applyConfig();
    updateProfileControls();

    ui->centralWidget->layout()->addWidget(&m_tableWidget);

    if (QCoreApplication::arguments().count() > 1)
    {
        QString fileName = QCoreApplication::arguments()[1];
        if (m_profile.loadFromFile(fileName))
        {
            m_profileFileName = fileName;
            updateProfileControls();
            m_config.setLastProfileDirectory(QDir(fileName).absolutePath());
            m_profile.setChanged(false);
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionStart_triggered()
{
    m_profile.start();
}

void MainWindow::on_actionStartFrom_triggered()
{
    if (m_tableWidget.currentActionIndex() != -1)
    {
        int msec = m_profile.action(m_tableWidget.currentActionIndex()).relativeTimeMSec();
        m_profile.startFromRelativeMSec(msec);
    }
}

void MainWindow::on_actionStop_triggered()
{
    m_profile.stop();
    m_profile.stopAudio();
}

void MainWindow::slotProfileViewChanged(QString currentView, QString relativeView)
{
    m_comm.setContent(currentView);
    ui->labelActualView->setText(" " + currentView);
    ui->labelRelativeView->setText(relativeView);
}

void MainWindow::slotProfileStarted()
{
    enableDisableProfileViews(false);
    ui->actionStart->setEnabled(false);
    ui->actionStop->setEnabled(true);
}

void MainWindow::slotProfileStopped()
{
    enableDisableProfileViews(true);
    ui->actionStart->setEnabled(true);
    ui->actionStop->setEnabled(false);
}

void MainWindow::slotProfileActionTaken(int actionIndex)
{
    m_tableWidget.setCurrentActionIndex(actionIndex);
}

void MainWindow::slotCommIsPresentChanged(bool isPresent)
{
    if (isPresent)
    {
        ui->labelConnected->setText("Ano");
        QPalette pal;
        pal.setColor(QPalette::WindowText, Qt::darkGreen);
        ui->labelConnected->setPalette(pal);
    }
    else
    {
        ui->labelConnected->setText("Ne");
        QPalette pal;
        pal.setColor(QPalette::WindowText, Qt::red);
        ui->labelConnected->setPalette(pal);
    }
}

void MainWindow::slotActionTableIndexChanged(int)
{
    updateProfileEditActionViews();
}

void MainWindow::slotActionTableDoubleClick(int)
{
    on_actionEditAction_triggered();
}

void MainWindow::on_actionAddAction_triggered()
{
    ProfileAction act;
    if (m_tableWidget.profileActions().count())
    {
        act = m_tableWidget.profileActions()[m_tableWidget.profileActions().count() - 1];
        act.setAudioFile("");
    }
    else
    {
        act.setRelativeTimeMSec(0);
    }
    m_actionDialog.setProfileAction(act);
    if (m_actionDialog.exec() == QDialog::Accepted)
    {
        m_profile.addAction(m_actionDialog.profileAction());
        m_tableWidget.setProfileActions(m_profile.actions());
    }
}

void MainWindow::on_actionEditAction_triggered()
{
    if (m_tableWidget.currentActionIndex() != -1)
    {
        m_actionDialog.setProfileAction(m_tableWidget.profileActions()[m_tableWidget.currentActionIndex()]);
        if (m_actionDialog.exec() == QDialog::Accepted)
        {
            m_profile.setAction(m_tableWidget.currentActionIndex(), m_actionDialog.profileAction());
            m_tableWidget.setProfileActions(m_profile.actions());
        }
    }
}

void MainWindow::on_actionDeleteAction_triggered()
{
    if (m_tableWidget.currentActionIndex() != -1)
    {
        m_profile.deleteAction(m_tableWidget.currentActionIndex());
        m_tableWidget.setProfileActions(m_profile.actions());
    }
}

void MainWindow::on_actionNewProfile_triggered()
{
    if (m_profile.hasChanged())
    {
        QMessageBox msgBox;
        msgBox.setText("Profil byl změněn.");
        msgBox.setInformativeText("Chcete uložit změny?");
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        msgBox.setWindowTitle("Uložení profilu");
        msgBox.setButtonText(QMessageBox::Save, "Uložit");
        msgBox.setButtonText(QMessageBox::Discard, "Neukládat");
        msgBox.setButtonText(QMessageBox::Cancel, "Storno");
        int ret = msgBox.exec();
        switch (ret) {
          case QMessageBox::Save:
              if (saveProfile())
                  clearProfile();
              break;
          case QMessageBox::Discard:
              clearProfile();
              break;
          case QMessageBox::Cancel:
              break;
          default:
              break;
        }
    }
    else
        clearProfile();
}

void MainWindow::on_actionSaveProfile_triggered()
{
    saveProfile();
}

void MainWindow::on_actionOpenProfile_triggered()
{
    if (m_profile.hasChanged())
    {
        QMessageBox msgBox;
        msgBox.setText("Profil byl změněn.");
        msgBox.setInformativeText("Chcete uložit změny?");
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        msgBox.setWindowTitle("Uložení profilu");
        msgBox.setButtonText(QMessageBox::Save, "Uložit");
        msgBox.setButtonText(QMessageBox::Discard, "Neukládat");
        msgBox.setButtonText(QMessageBox::Cancel, "Storno");
        int ret = msgBox.exec();
        switch (ret) {
          case QMessageBox::Save:
              if (saveProfile())
                  loadProfile();
              break;
          case QMessageBox::Discard:
              loadProfile();
              break;
          case QMessageBox::Cancel:
              break;
          default:
              break;
        }
    }
    else
        loadProfile();
}

void MainWindow::on_actionSaveProfileAs_triggered()
{
    saveProfile(true);
}

void MainWindow::on_comboDirection_currentIndexChanged(int index)
{
    if (index == 0)
        m_profile.setCountDirection(Profile::dirDown);
    else
        m_profile.setCountDirection(Profile::dirUp);
}

void MainWindow::on_spinStartMin_valueChanged(int val)
{
    m_profile.setStartMin(val);
}

void MainWindow::on_spinStartSec_valueChanged(int val)
{
    m_profile.setStartSec(val);
}

void MainWindow::on_spinStopMin_valueChanged(int val)
{
    m_profile.setStopMin(val);
}

void MainWindow::on_spinStopSec_valueChanged(int val)
{
    m_profile.setStopSec(val);
}

void MainWindow::on_actionExit_triggered()
{
    close();
}

void MainWindow::on_actionConfig_triggered()
{
    m_configDialog.setConfig(m_config);
    if (m_configDialog.exec() == QDialog::Accepted)
    {
        m_config = m_configDialog.config();
        applyConfig();
    }
}

void MainWindow::on_actionManu_l_triggered()
{
    showInGraphicalShell(QDir::currentPath() + "/manual");
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::about(this, "O aplikaci", "Časomíra, v" + QString::number(APP_VER_MAJOR) + "." + QString::number(APP_VER_MINOR) + "." + QString::number(APP_VER_PATCH) + "\n" +
                       APP_COPYRIGHT);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (m_profile.hasChanged())
    {
        QMessageBox msgBox;
        msgBox.setText("Profil byl změněn.");
        msgBox.setInformativeText("Chcete uložit změny?");
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        msgBox.setWindowTitle("Uložení profilu");
        msgBox.setButtonText(QMessageBox::Save, "Uložit");
        msgBox.setButtonText(QMessageBox::Discard, "Neukládat");
        msgBox.setButtonText(QMessageBox::Cancel, "Storno");
        int ret = msgBox.exec();
        switch (ret) {
          case QMessageBox::Save:
              if (saveProfile())
                  event->accept();
              else
                  event->ignore();
              break;
          case QMessageBox::Discard:
              event->accept();
              break;
          case QMessageBox::Cancel:
              event->ignore();
              break;
          default:
              break;
        }
    }
    else
        event->accept();
    m_config.saveToFile();
}

void MainWindow::showInGraphicalShell(const QString &pathIn)
{
    QDesktopServices::openUrl(QUrl("file:///" + pathIn, QUrl::TolerantMode));
}

void MainWindow::applyConfig()
{
    m_comm.close();
    m_comm.setPort(m_config.comPort());
    m_comm.open();
}

bool MainWindow::saveProfile(bool forceFileDialog)
{
    if ((forceFileDialog) || (m_profileFileName == ""))
    {
        if (m_profileFileName == "")
            m_profileFileName = m_config.lastProfileDirectory();
        QString fileName = QFileDialog::getSaveFileName(this,
            "Uložit profil", m_profileFileName,
            "Soubory profilů (*.tprof);;Všechny soubory (*)");
        if (fileName != "")
        {
            m_profileFileName = fileName;
            bool result = m_profile.saveToFile(fileName);
            if (result)
                m_config.setLastProfileDirectory(QDir(fileName).absolutePath());
            return result;
        }
        return false;
    }
    else
        return m_profile.saveToFile(m_profileFileName);
}

bool MainWindow::loadProfile()
{
    m_profileFileName = m_config.lastProfileDirectory();
    QString fileName = QFileDialog::getOpenFileName(this,
        "Otevřít profil", m_profileFileName,
        "Soubory profilů (*.tprof);;Všechny soubory (*)");
    if (fileName != "")
    {
        if (m_profile.loadFromFile(fileName))
        {
            m_profileFileName = fileName;
            updateProfileControls();
            m_config.setLastProfileDirectory(QDir(fileName).absolutePath());
            m_profile.setChanged(false);
            return true;
        }
        return false;
    }
    return false;
}

void MainWindow::clearProfile()
{
    m_profile.clear();
    updateProfileControls();
}

void MainWindow::updateProfileControls()
{
    if (m_profile.countDirection() == Profile::dirDown)
        ui->comboDirection->setCurrentIndex(0);
    else
        ui->comboDirection->setCurrentIndex(1);
    ui->spinStartMin->setValue(m_profile.startMin());
    ui->spinStartSec->setValue(m_profile.startSec());
    ui->spinStopMin->setValue(m_profile.stopMin());
    ui->spinStopSec->setValue(m_profile.stopSec());
    m_tableWidget.setProfileActions(m_profile.actions());
}

void MainWindow::enableDisableProfileViews(bool enable)
{
    m_tableWidget.setEnabled(enable);
    ui->comboDirection->setEnabled(enable);
    ui->spinStartMin->setEnabled(enable);
    ui->spinStartSec->setEnabled(enable);
    ui->spinStopMin->setEnabled(enable);
    ui->spinStopSec->setEnabled(enable);
    ui->btnAddAction->setEnabled(enable);
    ui->btnStartFrom->setEnabled(enable);
    ui->actionAddAction->setEnabled(enable);
    updateProfileEditActionViews();
}

void MainWindow::updateProfileEditActionViews()
{
    if ((!ui->btnAddAction->isEnabled()) || (m_tableWidget.currentActionIndex() == -1))
    {
        ui->btnRemoveAction->setEnabled(false);
        ui->btnEditAction->setEnabled(false);
        ui->actionDeleteAction->setEnabled(false);
        ui->actionEditAction->setEnabled(false);
        ui->actionStartFrom->setEnabled(false);
    }
    else
    {
        ui->btnRemoveAction->setEnabled(true);
        ui->btnEditAction->setEnabled(true);
        ui->actionDeleteAction->setEnabled(true);
        ui->actionEditAction->setEnabled(true);
        ui->actionStartFrom->setEnabled(true);
    }
}

