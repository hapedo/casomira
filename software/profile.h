#ifndef PROFILE_H
#define PROFILE_H

#include <QObject>
#include <QTimer>
#include <QString>
#include <QVector>
#include <QMediaPlayer>
#include "profileaction.h"

class Profile : public QObject
{
    Q_OBJECT
public:

    enum CountDirection
    {
        dirDown,
        dirUp
    };

    Profile();

    bool saveToFile(QString fileName);

    bool loadFromFile(QString fileName);

    bool hasChanged() const;

    void setChanged(bool changed);

    void clear();

    void setStartMin(int min);

    void setStartSec(int sec);

    void setStopMin(int min);

    void setStopSec(int sec);

    int startMin() const;

    int startSec() const;

    int stopMin() const;

    int stopSec() const;

    void setCountDirection(CountDirection dir);

    CountDirection countDirection() const;

    bool isRunning() const;

    void start();

    void startFromRelativeMSec(int msec);

    void stop();

    void stopAudio();

    QString currentView();

    QString currentRelativeView();

    int currentMinute() const;

    int currentSecond() const;

    int currentRelativeMinute();

    int currentRelativeSecond();

    void clearActions();

    void deleteAction(int index);

    void addAction(const ProfileAction& action);

    int actionCount() const;

    ProfileAction action(int index) const;

    void setAction(int index, const ProfileAction& action);

    const ProfileAction* actionById(int id) const;

    const QVector<ProfileAction>& actions() const;

signals:

    void signalCountingStarted();

    void signalCountingStopped();

    void signalActionTaken(int actionIndex);

    void signalViewChanged(QString currentView, QString relativeView);

private slots:

    void slotMainTimer();

private:

    Profile(const &Profile);

    Profile(const &&Profile);

    Profile& operator = (const &Profile);

    void sortByTime();

    void calculateAdditionalTime();

    void updateTime();

    void updateActions(bool init = false);

    qint64 totalPeriodMSec() const;

    qint64 totalPeriodMSecVisual() const;

    int currentMinuteOverflow() const;

    int currentMinute(bool& isNegative) const;

    bool checkViewUpdate();

    static QString formatNumber(int number, bool supressZero = true);

    int m_startMin;

    int m_startSec;

    int m_stopMin;

    int m_stopSec;

    CountDirection m_countDirection;

    bool m_isRunning;

    bool m_hasChanged;

    QTimer m_mainTimer;

    qint64 m_startTimestamp;

    qint64 m_startDeltaPeriod;

    qint64 m_additionalTime;

    qint64 m_stopTimestamp;

    qint64 m_stopVisualTimestamp;

    qint64 m_blinkTimestamp;

    qint64 m_currentActionTimestamp;

    int m_currentActionIndex;

    bool m_blinkOn;

    bool m_blinkEnabled;

    QVector<ProfileAction> m_actions;

    QMediaPlayer m_player;

};

#endif // PROFILE_H
