#ifndef ACTIONDIALOG_H
#define ACTIONDIALOG_H

#include <QDialog>
#include "profileaction.h"

namespace Ui {
class ActionDialog;
}

class ActionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ActionDialog(QWidget *parent = 0);

    void setProfileAction(const ProfileAction& action);

    ProfileAction profileAction() const;

    ~ActionDialog();

private slots:
    void on_btnOpenSound_clicked();

private:
    Ui::ActionDialog *ui;
};

#endif // ACTIONDIALOG_H
