#include "profile.h"
#include <QDateTime>
#include <QFile>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QFileInfo>
#include <QDir>
#include <math.h>
#include <algorithm>

using namespace std;

Profile::Profile() :
    m_startMin(7),
    m_startSec(0),
    m_stopMin(0),
    m_stopSec(0),
    m_countDirection(dirDown),
    m_isRunning(false),
    m_hasChanged(false)
{
    m_mainTimer.setInterval(100);
    m_mainTimer.setSingleShot(false);
    QObject::connect(&m_mainTimer, SIGNAL(timeout()),
                     this, SLOT(slotMainTimer()));
    m_mainTimer.start();
    m_blinkTimestamp = QDateTime::currentMSecsSinceEpoch() + 500;
    m_blinkOn = true;
    m_blinkEnabled = true;
    m_startDeltaPeriod = 0;
    m_currentActionIndex = 0;
}

bool Profile::saveToFile(QString fileName)
{
    QFile xmlFile(fileName);
    if (!xmlFile.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;

    QXmlStreamWriter writer(&xmlFile);
    writer.setAutoFormatting(true);
    writer.setAutoFormattingIndent(4);
    writer.writeStartDocument();
    writer.writeDTD("<!DOCTYPE CAPP>");

    QXmlStreamAttributes attrs;
    attrs.append("StartMin", QString::number(startMin()));
    attrs.append("StartSec", QString::number(startSec()));
    attrs.append("StopMin", QString::number(stopMin()));
    attrs.append("StopSec", QString::number(stopSec()));
    if (countDirection() == dirDown)
        attrs.append("Direction", "Down");
    else
        attrs.append("Direction", "Up");
    writer.writeStartElement("Profile");
    writer.writeAttributes(attrs);

    for_each(m_actions.begin(), m_actions.end(), [&](const ProfileAction& action){
       attrs.clear();
       attrs.append("RelTimeMSec", QString::number(action.relativeTimeMSec()));
       QFileInfo fi(action.audioFile());
       QDir dir(QFileInfo(fileName).absoluteDir());
       attrs.append("AudioFile", dir.relativeFilePath(action.audioFile()));
       writer.writeStartElement("Action");
       writer.writeAttributes(attrs);
       writer.writeEndElement();
    });

    writer.writeEndElement();
    writer.writeEndDocument();

    m_hasChanged = false;

    return true;
}

bool Profile::loadFromFile(QString fileName)
{
    QFile xmlFile(fileName);
    if (!xmlFile.open(QIODevice::ReadOnly | QIODevice::Text))
        return false;

    QXmlStreamReader reader(&xmlFile);

    clear();

    bool inProfile = false;
    while(!reader.atEnd())
    {
        if (reader.hasError())
            return false;
        QXmlStreamReader::TokenType token = reader.readNext();
        switch(token)
        {
        case QXmlStreamReader::StartElement:
            if (!inProfile)
            {
                if (reader.name().toString() == "Profile")
                {
                    inProfile = true;
                    if (reader.attributes().hasAttribute("StartMin"))
                        setStartMin(reader.attributes().value("StartMin").toInt());
                    if (reader.attributes().hasAttribute("StartSec"))
                        setStartSec(reader.attributes().value("StartSec").toInt());
                    if (reader.attributes().hasAttribute("StopMin"))
                        setStopMin(reader.attributes().value("StopMin").toInt());
                    if (reader.attributes().hasAttribute("StopSec"))
                        setStopSec(reader.attributes().value("StopSec").toInt());
                    if (reader.attributes().hasAttribute("Direction"))
                    {
                        if (reader.attributes().value("Direction").toString().toLower() == "down")
                            setCountDirection(dirDown);
                        else
                            setCountDirection(dirUp);
                    }
                }
            }
            else
            {
                if (reader.name().toString() == "Action")
                {
                    ProfileAction action;
                    if (reader.attributes().hasAttribute("RelTimeMSec"))
                        action.setRelativeTimeMSec(reader.attributes().value("RelTimeMSec").toInt());
                    if (reader.attributes().hasAttribute("AudioFile"))
                    {
                        QDir dir(QFileInfo(fileName).absoluteDir());
                        QFileInfo fi(reader.attributes().value("AudioFile").toString());
                        if (fi.isRelative())
                            action.setAudioFile(dir.absolutePath() + "/" + reader.attributes().value("AudioFile").toString());
                        else
                            action.setAudioFile(reader.attributes().value("AudioFile").toString());
                    }
                    m_actions.append(action);
                }
            }
            break;

        case QXmlStreamReader::EndElement:
            if (reader.name().toString() == "Profile")
                inProfile = false;
            break;

        default:
            break;
        }
    }

    m_hasChanged = false;

    return true;
}

bool Profile::hasChanged() const
{
    return m_hasChanged;
}

void Profile::setChanged(bool changed)
{
    m_hasChanged = changed;
}

void Profile::clear()
{
    setStartMin(7);
    setStartSec(0);
    setStopMin(0);
    setStopSec(0);
    setCountDirection(dirDown);
    m_actions.clear();
    m_hasChanged = false;
}

void Profile::setStartMin(int min)
{
    m_startMin = min;
    m_hasChanged = true;
}

void Profile::setStartSec(int sec)
{
    m_startSec = sec;
    m_hasChanged = true;
}

void Profile::setStopMin(int min)
{
    m_stopMin = min;
    m_hasChanged = true;
}

void Profile::setStopSec(int sec)
{
    m_stopSec = sec;
    m_hasChanged = true;
}

int Profile::startMin() const
{
    return m_startMin;
}

int Profile::startSec() const
{
    return m_startSec;
}

int Profile::stopMin() const
{
    return m_stopMin;
}

int Profile::stopSec() const
{
    return m_stopSec;
}

void Profile::setCountDirection(CountDirection dir)
{
    m_countDirection = dir;
    m_hasChanged = true;
}

Profile::CountDirection Profile::countDirection() const
{
    return m_countDirection;
}

bool Profile::isRunning() const
{
    return m_isRunning;
}

void Profile::start()
{
    m_blinkEnabled = false;
    m_player.stop();
    calculateAdditionalTime();
    m_startTimestamp = QDateTime::currentMSecsSinceEpoch();
    m_stopTimestamp = m_startTimestamp + totalPeriodMSec();
    m_stopVisualTimestamp = m_startTimestamp + totalPeriodMSecVisual();
    m_isRunning = true;
    m_currentActionIndex = 0;
    emit signalCountingStarted();
    emit signalViewChanged(currentView(), currentRelativeView());
    updateActions(true);
    updateTime();
}

void Profile::startFromRelativeMSec(int msec)
{
    m_blinkEnabled = false;
    m_player.stop();
    calculateAdditionalTime();
    m_startTimestamp = QDateTime::currentMSecsSinceEpoch() - msec;
    if (m_actions.count() && (m_actions[0].relativeTimeMSec() < 0))
        m_startTimestamp += m_actions[0].relativeTimeMSec();
    m_stopTimestamp = m_startTimestamp + totalPeriodMSec();
    m_stopVisualTimestamp = m_startTimestamp + totalPeriodMSecVisual();
    m_isRunning = true;
    m_currentActionIndex = 0;
    for(int i = 0; i < m_actions.count(); i++)
        if (m_actions[i].relativeTimeMSec() >= msec)
        {
            m_currentActionIndex = i;
            break;
        }
    emit signalCountingStarted();
    emit signalViewChanged(currentView(), currentRelativeView());
    updateActions(true);
    updateTime();
}

void Profile::stop()
{
    m_isRunning = false;
    m_blinkTimestamp = QDateTime::currentMSecsSinceEpoch() + 500;
    m_blinkOn = true;
    m_blinkEnabled = true;
    emit signalCountingStopped();
    emit signalViewChanged(currentView(), currentRelativeView());
}

void Profile::stopAudio()
{
    m_player.stop();
}

QString Profile::currentView()
{
    QString result;
    if (!m_blinkEnabled)
    {
        result = formatNumber(currentMinute()) + ":" + formatNumber(currentSecond(), false);
    }
    else
    {
        if (m_blinkOn)
            result = formatNumber(currentMinute()) + ":" + formatNumber(currentSecond(), false);
        else
            result = "  :  ";
    }
    return result;
}

QString Profile::currentRelativeView()
{
    int sec = currentRelativeSecond();
    int min = currentRelativeMinute();
    QString sign = "+";
    if (sec < 0)
        sign = "-";
    if (min < 0)
        sign = "-";
    return sign + formatNumber(abs(min)) + ":" + formatNumber(abs(sec), false);
}

int Profile::currentMinute() const
{
    bool isNegative;
    return currentMinute(isNegative);
}

int Profile::currentMinute(bool& isNegative) const
{
    int result = 0;
    if (!m_isRunning)
        result = m_stopMin;
    else
    {
        qint64 ts = QDateTime::currentMSecsSinceEpoch();
        if (ts < m_startTimestamp + m_startDeltaPeriod)
            result = m_startMin;
        else
        {
            if (ts >= m_stopVisualTimestamp)
            {
                result = m_stopMin;
            }
            else
            {
                if (m_countDirection == dirUp)
                    result = m_startMin + currentMinuteOverflow() + (ts - m_startTimestamp - m_startDeltaPeriod) / 60000;
                else
                    result = m_startMin - currentMinuteOverflow() - ((ts - m_startTimestamp - m_startDeltaPeriod) / 60000);
            }
        }
    }
    if (result < 0)
    {
        result = 0;
        isNegative = true;
    }
    else
        isNegative = false;
    return result;
}

int Profile::currentSecond() const
{
    bool isNegative;
    currentMinute(isNegative);
    int result = 0;
    if (!m_isRunning)
        result = m_stopSec;
    else
    {
        qint64 ts = QDateTime::currentMSecsSinceEpoch();
        if (ts < m_startTimestamp + m_startDeltaPeriod)
            result = m_startSec;
        else
        {
            if (ts >= m_stopVisualTimestamp)
            {
                result = m_stopSec;
            }
            else
            {
                if (m_countDirection == dirUp)
                    result = (m_startSec + (((ts - m_startTimestamp - m_startDeltaPeriod) % 60000) / 1000)) % 60;
                else
                {
                    int r = (m_startSec - (((ts - m_startTimestamp - m_startDeltaPeriod) % 60000) / 1000)) % 60;
                    if (r < 0)
                        r = 60 + r;
                    result = r;
                }
            }
        }
    }
    if ((result < 0) || (isNegative))
        result = 0;
    return result;
}

int Profile::currentRelativeMinute()
{
    if (!m_isRunning)
        return 0;

    int ts = (QDateTime::currentMSecsSinceEpoch() - m_startTimestamp) / 1000;
    ts -= m_startDeltaPeriod / 1000;
    int sec = abs(ts % 60);
    ts = ts / 60;
    if (ts < 0)
        ts -= sec / 60;
    return ts;
}

int Profile::currentRelativeSecond()
{
    if (!m_isRunning)
        return 0;

    int ts = (QDateTime::currentMSecsSinceEpoch() - m_startTimestamp) / 1000;
    ts -= m_startDeltaPeriod / 1000;
    ts = ts % 60;
    return ts;
}

void Profile::clearActions()
{
    if (m_isRunning)
        return;
    m_actions.clear();
    m_hasChanged = true;
}

void Profile::deleteAction(int index)
{
    if (m_isRunning)
        return;
    if (index >= m_actions.count())
        return;
    m_actions.removeAt(index);
    m_hasChanged = true;
}

void Profile::addAction(const ProfileAction& action)
{
    if (m_isRunning)
        return;
    m_actions.append(action);
    sortByTime();
    m_hasChanged = true;
}

int Profile::actionCount() const
{
    return m_actions.count();
}

ProfileAction Profile::action(int index) const
{
    return m_actions.at(index);
}

void Profile::setAction(int index, const ProfileAction& action)
{
    if (m_isRunning)
        return;
    if (index >= m_actions.count())
        return;
    m_actions[index] = action;
    sortByTime();
    m_hasChanged = true;
}

const ProfileAction* Profile::actionById(int id) const
{
    find_if(begin(m_actions), end(m_actions), [&](const ProfileAction& act) {
        if (act.id() == id)
            return true;
        return false;
    });
    return nullptr;
}

const QVector<ProfileAction>& Profile::actions() const
{
    return m_actions;
}

void Profile::sortByTime()
{
    sort(begin(m_actions), end(m_actions), [](const ProfileAction& p1, const ProfileAction& p2) {
        return p1.relativeTimeMSec() < p2.relativeTimeMSec();
    });
}

void Profile::updateTime()
{
    if (!m_isRunning)
        return;
    //qint64 ts = QDateTime::currentMSecsSinceEpoch();
    if ((currentMinute() == m_stopMin) && (currentSecond() == m_stopSec) && (!m_blinkEnabled))
    {
        m_blinkEnabled = true;
        m_blinkOn = true;
    }

    if (m_currentActionIndex >= m_actions.count())
    {
        stop();
    }
}

void Profile::calculateAdditionalTime()
{
    m_additionalTime = 0;
    if (m_actions.count())
    {
        qint64 start = (m_startMin * 60 + m_startSec) * 1000;
        qint64 stop = (m_stopMin * 60 + m_stopSec) * 1000;
        if (start > stop)
            m_additionalTime = m_actions[m_actions.count() - 1].relativeTimeMSec() - (start - stop);
        else
            m_additionalTime = m_actions[m_actions.count() - 1].relativeTimeMSec() - (stop - start);
        if (m_additionalTime < 0)
            m_additionalTime = 0;
    }
}

void Profile::updateActions(bool init)
{
    if (init)
    {
        calculateAdditionalTime();
        if ((m_actions.count()) && (m_actions.at(0).relativeTimeMSec() < 0))
            m_startDeltaPeriod = abs(m_actions.at(0).relativeTimeMSec());
        if (m_actions.count() == 0)
            m_currentActionTimestamp = m_stopTimestamp + 1000;
        else
            m_currentActionTimestamp = m_startTimestamp + m_startDeltaPeriod + m_actions.at(0).relativeTimeMSec();
        return;
    }

    if (!m_isRunning)
        return;

    if ((m_actions.count() == 0) || (m_currentActionIndex >= m_actions.count()))
        m_currentActionTimestamp = m_stopTimestamp + 1000;
    else
    {
        if (QDateTime::currentMSecsSinceEpoch() >= m_currentActionTimestamp)
        {
            QString f = m_actions[m_currentActionIndex].audioFile();
            if (f != "")
            {
                m_player.stop();
                m_player.setMedia(QUrl::fromLocalFile(f));
                m_player.setVolume(100);
                m_player.play();
            }
            emit signalActionTaken(m_currentActionIndex);
            m_currentActionIndex++;
            if (m_currentActionIndex >= m_actions.count())
                m_currentActionTimestamp = m_stopTimestamp + 1000;
            else
                m_currentActionTimestamp = m_startTimestamp + m_startDeltaPeriod + m_actions.at(m_currentActionIndex).relativeTimeMSec();
        }
    }
}

qint64 Profile::totalPeriodMSec() const
{
    qint64 start = (m_startMin * 60 + m_startSec) * 1000;
    qint64 stop = (m_stopMin * 60 + m_stopSec) * 1000;
    qint64 result = 0;
    if (m_countDirection == dirDown)
    {
        if (start > stop)
            result = start - stop + m_startDeltaPeriod;
    }
    else
    {
        if (stop > start)
            result = stop - start + m_startDeltaPeriod;
    }

    return result + m_additionalTime;
}

qint64 Profile::totalPeriodMSecVisual() const
{
    qint64 start = (m_startMin * 60 + m_startSec) * 1000;
    qint64 stop = (m_stopMin * 60 + m_stopSec) * 1000;
    qint64 result = 0;
    if (m_countDirection == dirDown)
    {
        if (start > stop)
            result = start - stop + m_startDeltaPeriod;
    }
    else
    {
        if (stop > start)
            result = stop - start + m_startDeltaPeriod;
    }

    return result;
}

int Profile::currentMinuteOverflow() const
{
    if (!m_isRunning)
        return 0;
    else
    {
        qint64 ts = QDateTime::currentMSecsSinceEpoch();
        if (ts < m_startDeltaPeriod + m_startDeltaPeriod)
            return 0;
        else
            if (m_countDirection == dirUp)
                return (m_startSec + (((ts - m_startTimestamp - m_startDeltaPeriod) % 60000) / 1000)) / 60;
            else
            {
                int r = m_startSec - (((ts - m_startTimestamp - m_startDeltaPeriod) % 60000) / 1000);
                if (r < 0)
                    r = 1 + abs(r / 60);
                else
                    r = r / 60;
                return r;
            }
    }
}

bool Profile::checkViewUpdate()
{
    static int lastMin = 0;
    static int lastSec = 0;
    static int lastRelMin = 0;
    static int lastRelSec = 0;

    qint64 ts = QDateTime::currentMSecsSinceEpoch();
    bool result = false;
    if ((m_blinkEnabled) && (ts >= m_blinkTimestamp))
    {
        m_blinkTimestamp = ts + 500;
        m_blinkOn = !m_blinkOn;
        result = true;
    }

    int cm = currentMinute();
    int cs = currentSecond();
    int crm = currentRelativeMinute();
    int crs = currentRelativeSecond();

    if (cm != lastMin)
        result = true;
    if (cs != lastSec)
        result = true;
    if (crm != lastRelMin)
        result = true;
    if (crs != lastRelSec)
        result = true;

    lastMin = cm;
    lastSec = cs;
    lastRelMin = crm;
    lastRelSec = crs;

    return result;
}

QString Profile::formatNumber(int number, bool supressZero)
{
    if (number < 10)
    {
        if (supressZero)
            return " " + QString::number(number);
        else
            return "0" + QString::number(number);
    }
    else if (number < 100)
        return QString::number(number);
    else
        return "--";
}

void Profile::slotMainTimer()
{
    updateTime();
    updateActions();
    if (checkViewUpdate())
        emit signalViewChanged(currentView(), currentRelativeView());
}
