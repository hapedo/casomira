#ifndef CONFIG_H
#define CONFIG_H

#include <QObject>
#include <QString>

class Config
{
public:
    Config();

    QString comPort() const;

    void setComPort(const QString& comPort);

    QString lastProfileDirectory();

    void setLastProfileDirectory(QString directory);

    bool loadFromFile();

    bool saveToFile();

private:

    QString m_comPort;

    QString m_lastProfileDirectory;
};

#endif // CONFIG_H
