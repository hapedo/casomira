#include "profileaction.h"

int ProfileAction::m_idCounter = 0;

ProfileAction::ProfileAction() :
    m_id(++m_idCounter),
    m_relativeTimeMSec(0),
    m_audioFile("")
{

}

ProfileAction::ProfileAction(const ProfileAction& action) :
    m_id(action.m_id),
    m_relativeTimeMSec(action.m_relativeTimeMSec),
    m_audioFile(action.m_audioFile)
{

}

ProfileAction& ProfileAction::operator = (const ProfileAction& action)
{
    m_id = action.m_id;
    m_relativeTimeMSec = action.m_relativeTimeMSec;
    m_audioFile = action.m_audioFile;
    return *this;
}

int ProfileAction::id() const
{
    return m_id;
}

void ProfileAction::setRelativeTimeMSec(int relativeTimeMSec)
{
    m_relativeTimeMSec = relativeTimeMSec;
}

int ProfileAction::relativeTimeMSec() const
{
    return m_relativeTimeMSec;
}

void ProfileAction::setAudioFile(QString audioFilePath)
{
    m_audioFile = audioFilePath;
}

QString ProfileAction::audioFile() const
{
    return m_audioFile;
}

QString ProfileAction::relativeTimeMSecToString(int time)
{
    bool negative = (time < 0);
    if (negative)
        time = -time;
    int sec = (time / 1000) % 60;
    int min = (time / 60000);
    QString res = QString::number(min) + ":";
    if (sec < 10)
        res += "0" + QString::number(sec);
    else
        res += QString::number(sec);
    if (negative)
        res = "-" + res;
    else
        res = "+" + res;
    return res;
}

int ProfileAction::stringToRelativeTimeMSec(const QString& str)
{
    bool negative = false;
    QString s;
    for(int i = 0; i < str.length(); i++)
    {
        if (((str[i].toLatin1() >= '0') && (str[i].toLatin1() <= '9')) ||
             (str[i].toLatin1() == '+') ||
             (str[i].toLatin1() == '-') ||
             (str[i].toLatin1() == ':'))
            s = s + str[i];
    }
    s.replace(" ", "");
    if (s.length() < 1)
        return 0;
    if (s[0].toLatin1() == '-')
        negative = true;
    int n = s.indexOf(":");
    QString min;
    QString sec = "0";
    if (n != -1)
    {
        min = s.mid(0, n);
        sec = s.mid(n + 1);
    }
    else
        min = s;
    int res = min.toInt() * 60000 + sec.toInt() * 1000;
    if (negative)
        res = -res;
    return res;
}


