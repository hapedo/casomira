#include "configdialog.h"
#include <QPushButton>
#include "ui_configdialog.h"
#include "comm.h"

ConfigDialog::ConfigDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfigDialog)
{
    ui->setupUi(this);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setText("OK");
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Storno");
}

ConfigDialog::~ConfigDialog()
{
    delete ui;
}

void ConfigDialog::setConfig(const Config& config)
{
    refreshPortList();
    ui->comboComPort->setCurrentText(config.comPort());
}

Config ConfigDialog::config() const
{
    Config conf;
    conf.setComPort(ui->comboComPort->currentText());
    return conf;
}

void ConfigDialog::refreshPortList()
{
    QString currStr = ui->comboComPort->currentText();
    ui->comboComPort->clear();
    QStringList list = Comm::availablePorts();
    ui->comboComPort->addItems(list);
    ui->comboComPort->setCurrentText(currStr);
}

void ConfigDialog::on_toolButton_clicked()
{
    refreshPortList();
}
