#ifndef _APP_CONFIG_H_
#define _APP_CONFIG_H_

#define APP_VER_MAJOR           0
#define APP_VER_MINOR           1
#define APP_VER_PATCH           1

#define APP_COPYRIGHT           "Copyright (c) 2022 Petr Hapal"

#endif // _APP_CONFIG_H_
