#ifndef COMM_H
#define COMM_H

#include <cstdint>
#include <QObject>
#include <QSerialPort>
#include <QTimer>
#include <QByteArray>

class Comm : public QObject
{
    Q_OBJECT
public:
    Comm();

    void setPort(const QString& port);

    static QStringList availablePorts();

    bool open();

    bool isOpened() const;

    void close();

    bool isPresent() const;

    void setContent(const QString& content);

signals:

    void signalIsPresentChanged(bool isPresent);

private slots:

    void updateDisplay();

    void rxTimeout();

    void presentTimeout();

    void presentCheckTimer();

    void portDataReady();

    void setPresent(bool isPresent);

private:

    static const uint8_t SEG_BIT_A              = 0x01;
    static const uint8_t SEG_BIT_B              = 0x02;
    static const uint8_t SEG_BIT_C              = 0x04;
    static const uint8_t SEG_BIT_D              = 0x08;
    static const uint8_t SEG_BIT_E              = 0x10;
    static const uint8_t SEG_BIT_F              = 0x20;
    static const uint8_t SEG_BIT_G              = 0x40;
    static const uint8_t SEG_BIT_H              = 0x80;

    static const uint8_t NUMBERS[10];

    Comm(const Comm&);

    Comm& operator = (const Comm&);

    uint8_t convert(char ch);

    QByteArray toHex(uint8_t val);

    QSerialPort m_port;

    bool m_isPresent;

    uint8_t m_content[4];

    QTimer m_updateTimer;

    QTimer m_rxTimeout;

    QTimer m_presentTimeout;

    QTimer m_presentTimer;

    QString m_rxData;
};

#endif // COMM_H
