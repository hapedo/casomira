#ifndef TABLEWIDGETCONTROLLER_H
#define TABLEWIDGETCONTROLLER_H

#include <QObject>
#include <QTableWidget>
#include <QVector>
#include "profileaction.h"

class TableWidget : protected QTableWidget
{
    Q_OBJECT
public:
    TableWidget();

    TableWidget(QWidget *parent);

    void setProfileActions(const QVector<ProfileAction> &actions);

    const QVector<ProfileAction>& profileActions() const;

    void update();

    int currentActionIndex() const;

    void setCurrentActionIndex(int index);

    void setEnabled(bool enabled);

signals:

    void signalActionIndexChanged(int index);

    void signalActionDoubleClick(int index);

private slots:

    void slotItemSelectionChanged();

private:

    TableWidget(const TableWidget&);

    TableWidget& operator = (const TableWidget&);

    void resizeEvent(QResizeEvent *event);

    void mouseDoubleClickEvent(QMouseEvent *event);

    void createHeader();

    void createRow(int row);

    QVector<ProfileAction> m_actions;
};

#endif // TABLEWIDGETCONTROLLER_H
