#ifndef CONFIGDIALOG_H
#define CONFIGDIALOG_H

#include <QDialog>
#include "config.h"

namespace Ui {
class ConfigDialog;
}

class ConfigDialog : public QDialog
{
    Q_OBJECT
public:
    explicit ConfigDialog(QWidget *parent = 0);
    ~ConfigDialog();

    void setConfig(const Config& config);

    Config config() const;

private slots:
    void on_toolButton_clicked();

private:

    void refreshPortList();

    Ui::ConfigDialog *ui;
};

#endif // CONFIGDIALOG_H
