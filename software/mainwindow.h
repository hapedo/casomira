#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include "profile.h"
#include "tablewidget.h"
#include "comm.h"
#include "actiondialog.h"
#include "configdialog.h"
#include "config.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionStart_triggered();

    void on_actionStop_triggered();

    void slotProfileViewChanged(QString currentView, QString relativeView);

    void slotProfileStarted();

    void slotProfileStopped();

    void slotProfileActionTaken(int actionIndex);

    void slotCommIsPresentChanged(bool isPresent);

    void slotActionTableIndexChanged(int actionIndex);

    void slotActionTableDoubleClick(int actionIndex);

    void on_actionAddAction_triggered();

    void on_actionEditAction_triggered();

    void on_actionDeleteAction_triggered();

    void on_actionNewProfile_triggered();

    void on_actionSaveProfile_triggered();

    void on_actionSaveProfileAs_triggered();

    void on_comboDirection_currentIndexChanged(int index);

    void on_spinStartMin_valueChanged(int arg1);

    void on_spinStartSec_valueChanged(int arg1);

    void on_spinStopMin_valueChanged(int arg1);

    void on_spinStopSec_valueChanged(int arg1);

    void on_actionOpenProfile_triggered();

    void on_actionExit_triggered();

    void on_actionConfig_triggered();

    void on_actionAbout_triggered();

    void on_actionStartFrom_triggered();

    void on_actionManu_l_triggered();

private:

    void closeEvent(QCloseEvent *event);

    void showInGraphicalShell(const QString &pathIn);

    void applyConfig();

    bool saveProfile(bool forceFileDialog = false);

    bool loadProfile();

    void clearProfile();

    void updateProfileControls();

    void enableDisableProfileViews(bool enable);

    void updateProfileEditActionViews();

    Ui::MainWindow *ui;

    Profile m_profile;

    Config m_config;

    TableWidget m_tableWidget;

    Comm m_comm;

    ActionDialog m_actionDialog;

    ConfigDialog m_configDialog;

    QString m_profileFileName;

};

#endif // MAINWINDOW_H
