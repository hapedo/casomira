#include "tablewidget.h"
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QPushButton>
#include <algorithm>

using namespace std;

TableWidget::TableWidget() :
    QTableWidget()
{
    connect(this, SIGNAL(itemSelectionChanged()),
            this, SLOT(slotItemSelectionChanged()));
    update();
}

TableWidget::TableWidget(QWidget *parent) :
    QTableWidget(parent)
{
    update();
}

void TableWidget::setProfileActions(const QVector<ProfileAction> &actions)
{
    m_actions = actions;
    setRowCount(m_actions.count());
    for(int i = 0; i < m_actions.count(); i++)
        createRow(i);
}

const QVector<ProfileAction>& TableWidget::profileActions() const
{
    return m_actions;
}

void TableWidget::update()
{
    createHeader();
    setRowCount(m_actions.count());
    for(int i = 0; i < m_actions.count(); i++)
        createRow(i);
}

int TableWidget::currentActionIndex() const
{
    return currentRow();
}

void TableWidget::setCurrentActionIndex(int index)
{
    setCurrentCell(index, 0);
}

void TableWidget::setEnabled(bool enabled)
{
    QTableWidget::setEnabled(enabled);
}

void TableWidget::slotItemSelectionChanged()
{
    emit signalActionIndexChanged(currentRow());
}

void TableWidget::resizeEvent(QResizeEvent *)
{
    setColumnWidth(0, width() / 3);
}

void TableWidget::mouseDoubleClickEvent(QMouseEvent *)
{
    if (currentRow() != -1)
        emit signalActionDoubleClick(currentActionIndex());
}

void TableWidget::createHeader()
{
    setRowCount(1);
    setColumnCount(2);
    horizontalHeader()->setStretchLastSection(true);
    horizontalHeader()->setHighlightSections(false);
    horizontalHeader()->show();
    verticalHeader()->hide();
    setHorizontalHeaderItem(0, new QTableWidgetItem("Relativní čas"));
    setHorizontalHeaderItem(1, new QTableWidgetItem("Zvukový soubor"));
    setSelectionBehavior(SelectionBehavior::SelectRows);
    setEditTriggers(EditTrigger::NoEditTriggers);
    setSelectionMode(SelectionMode::SingleSelection);
    setStyleSheet ( "QTableWidget {outline: 0;}" );
}

void TableWidget::createRow(int index)
{
/*    if (m_actions.count() < index + 1)
        return;*/
    if (rowCount() < index + 1)
        setRowCount(index + 1);

    QWidget* widget = new QWidget;
    QHBoxLayout *layout = new QHBoxLayout(this);
    QLabel* labelTime = new QLabel(ProfileAction::relativeTimeMSecToString(m_actions[index].relativeTimeMSec()));
    layout->addWidget(labelTime);
    layout->setSpacing(6);
    layout->setContentsMargins(6, 0, 0, 0);
    widget->setLayout(layout);
    setCellWidget(index, 0, widget);


    widget = new QWidget;
    layout = new QHBoxLayout(this);
    QLabel* labelFile = new QLabel(m_actions[index].audioFile());
    layout->addWidget(labelFile);
    layout->setSpacing(6);
    layout->setContentsMargins(6, 0, 0, 0);
    widget->setLayout(layout);

    setCellWidget(index, 1, widget);
}
