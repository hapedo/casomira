#include "actiondialog.h"
#include "ui_actiondialog.h"
#include <QFileDialog>
#include <QPushButton>

ActionDialog::ActionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ActionDialog)
{
    ui->setupUi(this);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setText("OK");
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Storno");

}

ActionDialog::~ActionDialog()
{
    delete ui;
}

void ActionDialog::setProfileAction(const ProfileAction& action)
{
    ui->editTime->setText(ProfileAction::relativeTimeMSecToString(action.relativeTimeMSec()));
    ui->editSound->setText(action.audioFile());
}

ProfileAction ActionDialog::profileAction() const
{
    ProfileAction act;
    act.setRelativeTimeMSec(ProfileAction::stringToRelativeTimeMSec(ui->editTime->text()));
    act.setAudioFile(ui->editSound->text());
    return act;
}


void ActionDialog::on_btnOpenSound_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        "Otevřít zvuk", ui->editSound->text(), "Zvuky (*.mp3 *.wav);;Všechny soubory (*.*)");
    if (fileName != "")
        ui->editSound->setText(fileName);
}
