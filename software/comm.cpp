#include "comm.h"
#include <QSerialPortInfo>
#include <algorithm>

using namespace  std;

const uint8_t Comm::NUMBERS[10] = {
        SEG_BIT_A | SEG_BIT_B | SEG_BIT_C | SEG_BIT_D | SEG_BIT_E | SEG_BIT_F,
        SEG_BIT_B | SEG_BIT_C,
        SEG_BIT_A | SEG_BIT_B | SEG_BIT_D | SEG_BIT_E | SEG_BIT_G,
        SEG_BIT_A | SEG_BIT_B | SEG_BIT_C | SEG_BIT_D | SEG_BIT_G,
        SEG_BIT_B | SEG_BIT_C | SEG_BIT_F | SEG_BIT_G,
        SEG_BIT_A | SEG_BIT_C | SEG_BIT_D | SEG_BIT_F | SEG_BIT_G,
        SEG_BIT_A | SEG_BIT_C | SEG_BIT_D | SEG_BIT_E | SEG_BIT_F | SEG_BIT_G,
        SEG_BIT_A | SEG_BIT_B | SEG_BIT_C,
        SEG_BIT_A | SEG_BIT_B | SEG_BIT_C | SEG_BIT_D | SEG_BIT_E | SEG_BIT_F | SEG_BIT_G,
        SEG_BIT_A | SEG_BIT_B | SEG_BIT_C | SEG_BIT_D | SEG_BIT_F | SEG_BIT_G
    };


Comm::Comm() :
    m_isPresent{false}
{
    for(int i = 0; i < 4; i++)
        m_content[i] = 0;
    connect(&m_updateTimer, SIGNAL(timeout()),
            this, SLOT(updateDisplay()));
    connect(&m_presentTimeout, SIGNAL(timeout()),
            this, SLOT(presentTimeout()));
    connect(&m_presentTimer, SIGNAL(timeout()),
            this, SLOT(presentCheckTimer()));
    connect(&m_port, SIGNAL(readyRead()),
            this, SLOT(portDataReady()));

    m_updateTimer.setInterval(100);
    m_updateTimer.setSingleShot(false);
    m_updateTimer.start();
    m_rxTimeout.setInterval(200);
    m_rxTimeout.setSingleShot(true);
    m_presentTimeout.setInterval(1000);
    m_presentTimeout.setSingleShot(false);
    m_presentTimeout.start();
    m_presentTimer.setInterval(250);
    m_presentTimer.setSingleShot(false);
    m_presentTimer.start();
}

void Comm::setPort(const QString& port)
{
    m_port.setPortName(port);
}

QStringList Comm::availablePorts()
{
    QStringList ports;
    QList<QSerialPortInfo> list = QSerialPortInfo::availablePorts();
    for_each(list.begin(), list.end(), [&](const QSerialPortInfo& p) {
       ports.append(p.portName());
    });
    return ports;
}

bool Comm::open()
{
    m_port.setBaudRate(19200);
    m_port.setParity(QSerialPort::NoParity);
    m_port.setStopBits(QSerialPort::OneStop);
    return m_port.open(QIODevice::ReadWrite);
}

bool Comm::isOpened() const
{
    return m_port.isOpen();
}

void Comm::close()
{
    m_port.close();
    setPresent(false);
}

bool Comm::isPresent() const
{
    return m_isPresent;
}

void Comm::setContent(const QString& content)
{
    if (content.length() < 5)
        return;
    m_content[0] = convert(content[0].toLatin1());
    m_content[1] = convert(content[1].toLatin1());
    if (content[2] == ':')
        m_content[1] |= SEG_BIT_H;
    m_content[2] = convert(content[3].toLatin1());
    m_content[3] = convert(content[4].toLatin1());
}

uint8_t Comm::convert(char ch)
{
    uint8_t res = 0;
    if (ch == '-')
        res = SEG_BIT_G;
    else if ((ch >= '0') && (ch <= '9'))
        res = NUMBERS[ch - '0'];
    return res;
}

void Comm::updateDisplay()
{
    if (!isOpened())
        return;
    QByteArray data;
    data.append('>');
    data.append('S');
    data.append('!');
    for(int i = 0; i < 4; i++)
        data.append(toHex(m_content[i]));
    data.append(';');
    m_port.write(data);
}

void Comm::rxTimeout()
{
    m_rxData = "";
}

void Comm::presentTimeout()
{
    close();
    setPresent(false);
}

void Comm::presentCheckTimer()
{
    if (!isOpened())
    {
        open();
        return;
    }
    QByteArray data;
    data.append('>');
    data.append('P');
    data.append('?');
    data.append(';');
    m_port.write(data);
}

void Comm::portDataReady()
{
    QByteArray data = m_port.readAll();
    m_rxTimeout.start();
    m_rxData.append(data);

    if (m_rxData.count() > 20)
        m_rxData = m_rxData.mid(m_rxData.count() - 20);

    int i1 = m_rxData.indexOf('>');
    if (i1 != -1)
    {
        int i2 = m_rxData.indexOf(';');
        if (i2 != -1)
        {
            QString resp(m_rxData.mid(i1 + 1, i2 - i1 - 1));
            m_rxData = m_rxData.mid(i2 + 1);
            char cmd = resp[0].toLatin1();
            bool query = (resp[1] == '?');
            resp = resp.mid(2);
            if (((cmd == 'S') || (cmd == 'P')) && (!query))
            {
                if (resp == "OK")
                {
                    setPresent(true);
                    m_presentTimeout.start();
                }
            }
        }
    }
}

void Comm::setPresent(bool isPresent)
{
    bool last = m_isPresent;
    m_isPresent = isPresent;
    if (last != m_isPresent)
        emit signalIsPresentChanged(m_isPresent);
}

QByteArray Comm::toHex(uint8_t val)
{
    QByteArray res;
    uint8_t temp = val >> 4;
    if (temp >= 10)
        res.append('A' + temp - 10);
    else
        res.append('0' + temp);
    temp = val & 0x0f;
    if (temp >= 10)
        res.append('A' + temp - 10);
    else
        res.append('0' + temp);
    return res;
}
