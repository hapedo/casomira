#ifndef PROFILEACTION_H
#define PROFILEACTION_H

#include <QString>

class ProfileAction
{
public:

    ProfileAction();

    ProfileAction(const ProfileAction& action);

    ProfileAction& operator = (const ProfileAction& action);

    int id() const;

    void setRelativeTimeMSec(int relativeTimeMSec);

    int relativeTimeMSec() const;

    void setAudioFile(QString audioFilePath);

    QString audioFile() const;

    static QString relativeTimeMSecToString(int time);

    static int stringToRelativeTimeMSec(const QString& str);

private:

    static int m_idCounter;

    int m_id;

    int m_relativeTimeMSec;

    QString m_audioFile;

};

#endif // PROFILEACTION_H
