#include "config.h"
#include <QStandardPaths>
#include <QFile>
#include <QDir>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>

Config::Config()
{
    m_comPort = "COM1";
    m_lastProfileDirectory = QStandardPaths::writableLocation(QStandardPaths::HomeLocation) + "/Casomira/profily";
}

QString Config::comPort() const
{
    return m_comPort;
}

void Config::setComPort(const QString& comPort)
{
    m_comPort = comPort;
}

QString Config::lastProfileDirectory()
{
    QDir dir(m_lastProfileDirectory);
    if (!dir.exists())
    {
        m_lastProfileDirectory = QStandardPaths::writableLocation(QStandardPaths::HomeLocation) + "/Casomira/profily";
        dir = QDir(m_lastProfileDirectory);
        if (!dir.exists())
            m_lastProfileDirectory = QDir::currentPath() + "/profily";
    }
    return m_lastProfileDirectory;
}

void Config::setLastProfileDirectory(QString directory)
{
    QFileInfo fi(directory);
    if (fi.isFile())
        m_lastProfileDirectory = fi.absoluteDir().absolutePath();
    else
        m_lastProfileDirectory = fi.absoluteFilePath();
}

bool Config::loadFromFile()
{
    QString path = QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
    path += "/config.xml";

    QFile xmlFile(path);
    if (!xmlFile.open(QIODevice::ReadOnly | QIODevice::Text))
        return false;

    QXmlStreamReader reader(&xmlFile);

    while(!reader.atEnd())
    {
        if (reader.hasError())
            return false;
        QXmlStreamReader::TokenType token = reader.readNext();
        switch(token)
        {
        case QXmlStreamReader::StartElement:
            if (reader.name().toString() == "Config")
            {
                if (reader.attributes().hasAttribute("ComPort"))
                    setComPort(reader.attributes().value("ComPort").toString());
                if (reader.attributes().hasAttribute("LastProfileDirectory"))
                    setLastProfileDirectory(reader.attributes().value("LastProfileDirectory").toString());
            }
            break;

        case QXmlStreamReader::EndElement:
            break;

        default:
            break;
        }
    }

    return true;
}

bool Config::saveToFile()
{
    QString path = QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);

    QDir().mkdir(path);

    path += "/config.xml";

    QFile xmlFile(path);
    if (!xmlFile.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;

    QXmlStreamWriter writer(&xmlFile);
    writer.setAutoFormatting(true);
    writer.setAutoFormattingIndent(4);
    writer.writeStartDocument();
    writer.writeDTD("<!DOCTYPE CAPP>");

    QXmlStreamAttributes attrs;
    attrs.append("ComPort", comPort());
    attrs.append("LastProfileDirectory", lastProfileDirectory());
    writer.writeStartElement("Config");
    writer.writeAttributes(attrs);
    writer.writeEndElement();
    writer.writeEndDocument();

    return true;
}
